package com.example.list_maker

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListItemViewHolder (itemView: View): RecyclerView.ViewHolder(itemView) {
    val listItemTitle = itemView.findViewById<TextView>(R.id.list_item_title)
}