package com.example.list_maker

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    val listItemId = itemView.findViewById<TextView>(R.id.item_id)
    val listItemTitle = itemView.findViewById<TextView>(R.id.item_tittle)
}