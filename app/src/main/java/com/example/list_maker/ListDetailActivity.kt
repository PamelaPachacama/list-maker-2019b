package com.example.list_maker

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*

class ListDetailActivity : AppCompatActivity() {

    lateinit var list: TaskList
    lateinit var itemsRecyclerView: RecyclerView
    lateinit var addItemButton: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detail)
      //  setSupportActionBar(toolbar)

        list = intent.getParcelableExtra(MainActivity.INTENT_LIST_KEY)
        title = list.name

        itemsRecyclerView = findViewById(R.id.list_item_recyclerview)


        itemsRecyclerView.layoutManager = LinearLayoutManager(this)
        itemsRecyclerView.adapter = ListItemsAdapter(list.tasks)

        addItemButton = findViewById(R.id.add_item_button)
        addItemButton.setOnClickListener {showCreatedItemDialog()}
    }
    fun showCreatedItemDialog(){
        val itemEditText = EditText(this)
        itemEditText.inputType = InputType.TYPE_CLASS_TEXT
        AlertDialog.Builder(this)
            .setTitle(R.string.add_task_title)
            .setView(itemEditText)
            .setPositiveButton(R.string.add_task){ dialog, _->
                val task = itemEditText.text.toString()
                list.tasks.add(task)

                val recyclerViewAdapter = itemsRecyclerView.adapter as ListItemsAdapter
                recyclerViewAdapter.notifyItemInserted(list.tasks.size-1)
                dialog.dismiss()
            }
            .create()
            .show()

    }
    override fun onBackPressed(){
        val bundle = Bundle()
        bundle.putParcelable(MainActivity.INTENT_LIST_KEY, list)
        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)

        super.onBackPressed()
    }

}


